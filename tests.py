import unittest

from main import Processor, CExtensionLoader


class TestStrings(unittest.TestCase):

    def _run(self, val):
        return Processor.run_alg(CExtensionLoader, val)

    def test_string(self):
        self.assertEqual(self._run('qwe')[1], [])

    def test_negative(self):
        self.assertEqual(self._run(-12)[1], [])

    def test_zero(self):
        self.assertEqual(self._run(0)[1], [])

    def test_float(self):
        self.assertEqual(self._run(123.23)[1], [3, 41])

    def test_valid(self):
        self.assertEqual(self._run(457457)[1], [7, 11, 13, 457])


if __name__ == '__main__':
    unittest.main()

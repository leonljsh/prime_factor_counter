#include "alg.h"
#include <math.h>


int primeFactors(int n, struct list* factors) {
    if (n <= 0 ) {
        return 0;
    }
    int iter = 0;
    while (n % 2 == 0) {
        append(factors, 2);
        iter ++;
        n /= 2;
    }
    for (int i = 3; i <= sqrt(n); i += 2) {
        // While i divides n, print i and divide n
        while (n % i == 0) {
            append(factors, i);
            iter++;
            n /= i;
        }
    }
    if (n > 2) {
        append(factors, n);
        iter++;
    }
    return iter;
}

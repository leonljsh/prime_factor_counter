#include <Python.h>
#include "alg.h"


static PyObject *py_primeFactors(PyObject *self, PyObject *args) {
    int n;
    int arrLen;
    int iter = 0;

    if (!PyArg_ParseTuple(args, "i", &n))
        return NULL;

    struct list factors = (struct list) { .length = 0, .head = NULL, . tail = NULL};

    arrLen = primeFactors(n, &factors);

    PyObject* python_val = PyList_New(arrLen);
    if (arrLen == 0){
        return python_val;
    }
    struct node* current = factors.head;

    while (current != NULL) {
        PyObject* python_int = Py_BuildValue("i", current->val);
        PyList_SetItem(python_val, iter, python_int);
        iter++;
        current = current->next;
    }
    return python_val;
}


static PyMethodDef factorMethods[] = {
        {"primeFactors",     py_primeFactors,  METH_VARARGS, "Returns array of factors"},
        {NULL,         NULL,       0,            NULL}
};

static struct PyModuleDef factorModule = {
        PyModuleDef_HEAD_INIT,
        "factorModule",
        "Factors Module",
        -1,
        factorMethods
};

PyMODINIT_FUNC PyInit_factorModule(void) {
    return PyModule_Create(&factorModule);
}

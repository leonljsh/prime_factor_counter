import time
import argparse
from functools import partial
from typing import List, Tuple, TextIO
import concurrent.futures

from factorModule import primeFactors
from utils import get_hard_cpu_count, show_progress_bar

# setting to control slice size for pool executor
LINES_PER_THREAD = 1


class CExtensionLoader:
    @staticmethod
    def run(num: int) -> List[int]:
        return primeFactors(num)


class FileController:
    @classmethod
    def read_lines(cls, file, chunk_size):
        while True:
            data = file.readlines(chunk_size * LINES_PER_THREAD)
            if not data:
                break
            yield data

    @classmethod
    def get_lines_number(cls, file: TextIO) -> int:
        return sum(bl.count("\n") for bl in cls.line_blocks(file))

    @staticmethod
    def line_blocks(file, size=16):
        while True:
            b = file.read(size)
            if not b:
                file.seek(0)
                break
            if len(b) < size and b[-1] != '\n':
                b += '\n'
            yield b


class Processor:
    def __init__(self, inp: TextIO, out: TextIO, cpus: int = get_hard_cpu_count()):
        self.input_file = inp
        self.output_file = out
        self.cpus = cpus
        self.file_controller = FileController
        self.file_lines = self.count_lines()
        self.algorithm_controller = CExtensionLoader

    def count_lines(self):
        return self.file_controller.get_lines_number(self.input_file)

    def write_result(self, num: int, factors: List[int]):
        factors_str = ' * '.join(map(str, factors)) if factors else 'Invalid input'
        res = f"{num} = {factors_str}\n"
        self.output_file.write(res)

    def update_calc_status(self, i):
        show_progress_bar(i, self.file_lines)

    def calculate(self):
        with concurrent.futures.ProcessPoolExecutor(max_workers=self.cpus) as executor:
            percepted = 0
            self.update_calc_status(percepted)
            for lines in self.file_controller.read_lines(self.input_file, self.cpus):
                for num, factors in executor.map(partial(self.run_alg, self.algorithm_controller), lines):
                    percepted += 1
                    self.update_calc_status(percepted)
                    self.write_result(num, factors)

    @staticmethod
    def run_alg(algorithm_controller: CExtensionLoader, num: str) -> Tuple[int, List[int]]:
        try:
            num = int(num)
        except ValueError:
            return num, []
        return num, algorithm_controller.run(num)


def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('inp', type=argparse.FileType('r'), help='Input file')
    parser.add_argument('out', type=argparse.FileType('w'), help='Output file')

    args = parser.parse_args()
    start_time = time.time()

    proc = Processor(args.inp, args.out)
    print(f'Started with calculating in {proc.cpus} threads')
    print(f'Total numbers amount: {proc.file_lines}')
    proc.calculate()

    print("--- %s seconds ---" % (time.time() - start_time))


if __name__ == "__main__":
    main()

# setup.py
from distutils.core import setup, Extension

setup(name='factorModule',
      version='0.0.1',
      ext_modules=[Extension('factorModule',
                             ['module.c', 'alg.c']
                             )]
      )

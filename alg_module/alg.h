#ifndef DINO_TEST_ALG_H
#define DINO_TEST_ALG_H

#include <stdlib.h>

struct node {
    int val;
    struct node *next;
};

struct list {
    struct node *head;
    struct node *tail;
    int length;
};

//void append(struct list* currLst, int new_data)
inline void append(struct list* currLst, int new_data)
{
    struct node *new_node = malloc(sizeof(struct node));

    new_node->val = new_data;
    new_node->next = NULL;

    if (currLst->head == NULL) {
        currLst->head = new_node;
    } else {
        currLst->tail->next = new_node;
    }
    currLst->tail = new_node;
    currLst->length++;
}

int primeFactors(int, struct list*);

#endif //DINO_TEST_ALG_H

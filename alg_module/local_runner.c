#include <stdio.h>
#include "alg.h"


int* primeFactorsRes(int n) {
    int arrLen;

    struct list head = (struct list) { .length = 0, .head = NULL, . tail = NULL};

    arrLen = primeFactors(n, &head);

    int *array = malloc(sizeof(int) * arrLen);
    array[0] = arrLen;
    for (int i = 1; i < arrLen + 1; i++ ) {
        array[i] = n;
    }
    int i = 0;
    struct node* current = head.head;
    while (current != NULL) {
        array[i] = current->val;
        i++;
        current = current->next;
    }
    return array;
}

int main(int argc, char *argv[]){
    int *p;
    p = primeFactorsRes(2345);

    int it = *p;
    for (int i = 0; i < 10; i++ ) {
        printf( "%d\n", *(p + i));
    }
    return 0;
}